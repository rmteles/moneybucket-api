package com.teless.moneybucket.web.links;

import com.teless.moneybucket.model.domain.Transaction;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

/**
 * Created by Rafael <rmteles@gmail.com> on 07-Jan-17.
 */
@Component
public class TransactionLinks implements ResourceProcessor<Resource<Transaction>> {

    public static final String REL = "transaction";
    public static final String COLLECTION_REL = "transactions";
    public static final String FIND_BY_ACCOUNT_ID_PATH = "findByAccountId";
    public static final String FIND_BY_ACCOUNT_ID_REL = FIND_BY_ACCOUNT_ID_PATH;

    @Override
    public Resource<Transaction> process(Resource<Transaction> resource) {
        return resource;
    }
}
