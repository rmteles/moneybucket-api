package com.teless.moneybucket.web.links;

import com.teless.moneybucket.model.domain.User;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

/**
 * Created by Rafael <rmteles@gmail.com> on 07-Jan-17.
 */
@Component
@RequiredArgsConstructor
public class UserLinks implements ResourceProcessor<Resource<User>> {

    public static final String REL = "user";
    public static final String COLLECTION_REL = "users";

    @Override
    public Resource<User> process(Resource<User> resource) { return resource; }

}
