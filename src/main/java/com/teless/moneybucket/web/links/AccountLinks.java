package com.teless.moneybucket.web.links;

import com.teless.moneybucket.model.domain.Account;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

/**
 * Created by Rafael <rmteles@gmail.com> on 07-Jan-17.
 */
@Component
public class AccountLinks implements ResourceProcessor<Resource<Account>> {

    public static final String REL = "account";
    public static final String COLLECTION_REL = "accounts";
    public static final String FIND_BY_OWNER_ID_PATH = "findByOwnerId";
    public static final String FIND_BY_OWNER_ID_REL = FIND_BY_OWNER_ID_PATH;

    @Override
    public Resource<Account> process(Resource<Account> resource) {
        return resource;
    }
}
