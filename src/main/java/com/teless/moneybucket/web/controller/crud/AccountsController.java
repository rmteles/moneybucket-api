package com.teless.moneybucket.web.controller.crud;

import com.teless.moneybucket.web.links.AccountLinks;
import com.teless.moneybucket.web.links.TransactionLinks;
import com.teless.springarq.config.ApiProperties;
import com.teless.springarq.web.util.RedirectSupport;
import lombok.RequiredArgsConstructor;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by Rafael <rmteles@gmail.com> on 16-Jan-17.
 */
@RequiredArgsConstructor
@RepositoryRestController
public class AccountsController {

    private final ApiProperties apiProperties;

    @RequestMapping(method = GET, value = AccountLinks.REL + "/{id}/transactions")
    public String pagedUserAccounts(@PathVariable("id") Long id, HttpServletRequest request) {
        return new RedirectSupport()
                .to(apiProperties.getBasePath() + "/" + TransactionLinks.REL +  "/search/" + TransactionLinks.FIND_BY_ACCOUNT_ID_PATH)
                .addParameter("id", id)
                .addRequestParameters(request.getParameterMap())
                .build();
    }

}