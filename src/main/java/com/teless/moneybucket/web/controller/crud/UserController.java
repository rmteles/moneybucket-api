package com.teless.moneybucket.web.controller.crud;

import com.teless.moneybucket.model.domain.User;
import com.teless.moneybucket.model.repository.UserRepository;
import com.teless.moneybucket.web.links.AccountLinks;
import com.teless.moneybucket.web.links.UserLinks;
import com.teless.springarq.config.ApiProperties;
import com.teless.springarq.web.controller.CrudSupport;
import com.teless.springarq.web.util.RedirectSupport;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.rest.core.RepositoryConstraintViolationException;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by Rafael <rmteles@gmail.com> on 29-Dec-16.
 */
@Slf4j
@RequiredArgsConstructor
@RepositoryRestController
public class UserController {

    private final CrudSupport crudSupport;
    private final UserRepository repository;
    private final ApiProperties apiProperties;

    @RequestMapping(method = POST, path = UserLinks.REL)
    public
    @ResponseBody
    ResponseEntity<ResourceSupport> save(@RequestBody User user, BindingResult bindingResult,
                                         PersistentEntityResourceAssembler resourceAssembler) {
        try {
            return crudSupport.create(user, repository, resourceAssembler);
        } catch (DataIntegrityViolationException e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                if (log.isDebugEnabled()) log.error("Error creating user", e);

                bindingResult.rejectValue("email", "error.value.inUse");
                throw new RepositoryConstraintViolationException(bindingResult);
            } else {
                throw e;
            }
        }
    }

    @RequestMapping(method = GET, path = UserLinks.REL + "/{id}/accounts")
    public String pagedUserAccounts(@PathVariable("id") Long id, HttpServletRequest request) {
        return new RedirectSupport()
                .to(apiProperties.getBasePath() + "/" + AccountLinks.REL + "/search/" + AccountLinks.FIND_BY_OWNER_ID_PATH)
                .addParameter("id", id)
                .addRequestParameters(request.getParameterMap())
                .build();
    }

}
