package com.teless.moneybucket.web.converter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import org.javamoney.moneta.FastMoney;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.UnknownCurrencyException;
import java.io.IOException;

/**
 * Created by Rafael <rmteles@gmail.com> on 16-Jan-17.
 */
public class MonetaryAmountDeserializer extends JsonDeserializer<MonetaryAmount> {

    @Override
    public MonetaryAmount deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException, JsonProcessingException {

        MonetaryAmount monetaryAmount;

        JsonNode jsonNode = jsonParser.getCodec().readTree(jsonParser);
        if (jsonNode == null) {
            monetaryAmount = null;
        } else {
            JsonNode amountNode = jsonNode.get(MonetaryAmountSerializer.VALUE_KEY);
            JsonNode currencyNode = jsonNode.get(MonetaryAmountSerializer.CURRENCY_KEY);

            if (amountNode == null) {
                throw new RuntimeJsonMappingException("amount is required");
            }

            if (currencyNode == null) {
                throw new RuntimeJsonMappingException("currency is required");
            }

            CurrencyUnit currency = getCurrency(currencyNode);
            Double amount = getAmount(amountNode, currencyNode);

            monetaryAmount = FastMoney.of(amount, currency);
        }

        return monetaryAmount;
    }

    private CurrencyUnit getCurrency(JsonNode currencyNode) {
        try {
            return Monetary.getCurrency(currencyNode.asText());
        } catch (UnknownCurrencyException ex) {
            throw new RuntimeJsonMappingException("the currency: " + currencyNode.asText() + " is unknown");
        }
    }

    private Double getAmount(JsonNode amountNode, JsonNode currencyNode) {
        try {
            return Double.valueOf(amountNode.asText());
        } catch (NumberFormatException ex) {
            throw new RuntimeJsonMappingException("the amount: " + currencyNode.asText() + " is invalid");
        }
    }

}
