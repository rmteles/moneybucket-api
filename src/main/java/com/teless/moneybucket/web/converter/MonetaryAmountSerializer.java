package com.teless.moneybucket.web.converter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;

import javax.money.MonetaryAmount;
import java.io.IOException;

/**
 * Created by Rafael <rmteles@gmail.com> on 16-Jan-17.
 */
public class MonetaryAmountSerializer extends JsonSerializer<MonetaryAmount> {

    public static final String VALUE_KEY = "value";
    public static final String CURRENCY_KEY = "currency";

    @Override
    public void serialize(MonetaryAmount amount, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {

        jsonGenerator.writeTree(
                new ObjectMapper().createObjectNode()
                        .put(VALUE_KEY, amount.getNumber().toString())
                        .put(CURRENCY_KEY, amount.getCurrency().getCurrencyCode())
        );
    }

}
