package com.teless.moneybucket.config;

import com.teless.moneybucket.model.domain.Account;
import com.teless.moneybucket.model.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

/**
 * Created by rafael on 04/12/16.
 */
@Service
@RequiredArgsConstructor
public class AccountsLoader {

    private final AccountRepository accountRepository;

    @EventListener
    public void loadUsers(ApplicationReadyEvent event) {
        accountRepository.save(
                Account.builder()
                        .name("Acc1")
                        .description("Acc1 desc")
                        .build()
        );
    }

}
