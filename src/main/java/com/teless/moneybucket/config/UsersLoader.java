package com.teless.moneybucket.config;

import com.teless.moneybucket.model.domain.User;
import com.teless.moneybucket.model.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

/**
 * Created by rafael on 04/12/16.
 */
@Service
@RequiredArgsConstructor
public class UsersLoader {

    private final UserRepository userRepository;

    @EventListener
    public void loadUsers(ApplicationReadyEvent event) {
        for (int i = 0; i < 5; i++) {
            User user = User.builder()
                    .firstName("Rafael" + i)
                    .lastName("Teles" + i)
                    .email("rafael@email" + i)
                    .password("1a2b3c4d5e".toCharArray())
                    .build();

//            user.setAccounts(Arrays.asList(
//                    Account.builder().name("acc1").description("acc1 desc").owner(user).build(),
//                    Account.builder().name("acc2").description("acc2 desc").owner(user).build()
//            ));

            userRepository.save(user);
        }
    }

}
