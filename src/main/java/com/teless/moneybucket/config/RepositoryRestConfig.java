package com.teless.moneybucket.config;

import com.teless.moneybucket.model.validator.TransactionValidator;
import com.teless.moneybucket.model.validator.UserValidator;
import com.teless.springarq.config.ArqRepositoryRestConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 * Created by Rafael <rmteles@gmail.com> on 18-Dec-16.
 */
@Configuration
public class RepositoryRestConfig extends ArqRepositoryRestConfig {

    private static final String BEFORE_SAVE = "beforeSave";
    private static final String BEFORE_CREATE = "beforeCreate";

    @Bean
    @Primary
    Validator validator() {
        return new LocalValidatorFactoryBean();
    }

    @Override
    public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
        Validator beanValidator = validator();

        validatingListener.addValidator(BEFORE_CREATE, beanValidator);
        validatingListener.addValidator(BEFORE_SAVE, beanValidator);

        validatingListener.addValidator(BEFORE_CREATE, new UserValidator());
        validatingListener.addValidator(BEFORE_SAVE, new UserValidator());

        validatingListener.addValidator(BEFORE_CREATE, new TransactionValidator());
        validatingListener.addValidator(BEFORE_SAVE, new TransactionValidator());
    }

}
