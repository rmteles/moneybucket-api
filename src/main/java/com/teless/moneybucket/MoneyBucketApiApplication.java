package com.teless.moneybucket;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;

@EnableEncryptableProperties
@SpringBootApplication(scanBasePackages = "com.teless")
public class MoneyBucketApiApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(MoneyBucketApiApplication.class, args);

//		System.out.println("Let's inspect the beans provided by Spring Boot:");
//
//		String[] beanNames = ctx.getBeanDefinitionNames();
//		Arrays.sort(beanNames);
//		for (String beanName : beanNames) {
//			System.out.println(beanName);
//		}
	}

}
