package com.teless.moneybucket.model.repository.handler;

import com.teless.moneybucket.model.domain.User;
import org.springframework.data.rest.core.annotation.HandleBeforeLinkSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

/**
 * Created by Rafael <rmteles@gmail.com> on 20-Jan-17.
 */
@Component
@RepositoryEventHandler(User.class)
public class UserEventHandler {

    @HandleBeforeLinkSave
    public void handleLink(User user, Object linkedObject) {
        if (user.getAccounts() != null) {
            user.getAccounts().forEach(account -> account.setOwner(user));
        }
    }

}
