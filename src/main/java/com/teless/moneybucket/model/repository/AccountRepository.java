package com.teless.moneybucket.model.repository;

import com.teless.moneybucket.model.domain.Account;
import com.teless.moneybucket.web.links.AccountLinks;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * Created by rafael on 11/12/16.
 */
@RepositoryRestResource(collectionResourceRel = AccountLinks.COLLECTION_REL, path = AccountLinks.REL)
public interface AccountRepository extends PagingAndSortingRepository<Account, Long> {

    @RestResource(path = AccountLinks.FIND_BY_OWNER_ID_PATH, rel = AccountLinks.FIND_BY_OWNER_ID_REL)
    Page<Account> findByOwnerId(@Param("id") Long id, Pageable pageable);

}
