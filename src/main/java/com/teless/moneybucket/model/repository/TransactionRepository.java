package com.teless.moneybucket.model.repository;

import com.teless.moneybucket.model.domain.Transaction;
import com.teless.moneybucket.web.links.TransactionLinks;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * Created by rafael on 11/12/16.
 */
@RepositoryRestResource(collectionResourceRel = TransactionLinks.COLLECTION_REL, path = TransactionLinks.REL)
public interface TransactionRepository extends PagingAndSortingRepository<Transaction, Long> {

    @RestResource(path = TransactionLinks.FIND_BY_ACCOUNT_ID_PATH, rel = TransactionLinks.FIND_BY_ACCOUNT_ID_REL)
    Page<Transaction> findByAccountId(@Param("id") Long id, Pageable pageable);

}
