package com.teless.moneybucket.model.repository;

import com.teless.moneybucket.model.domain.User;
import com.teless.moneybucket.web.links.UserLinks;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by rafael on 11/12/16.
 */
@RepositoryRestResource(collectionResourceRel = UserLinks.COLLECTION_REL, path = UserLinks.REL)
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
}
