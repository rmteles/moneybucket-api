package com.teless.moneybucket.model.repository.handler;

import com.teless.moneybucket.model.domain.Transaction;
import com.teless.moneybucket.model.service.CurrencyUnitService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.rest.core.annotation.*;
import org.springframework.stereotype.Component;

import javax.money.CurrencyUnit;
import javax.money.convert.MonetaryConversions;

/**
 * Created by Rafael <rmteles@gmail.com> on 16-Jan-17.
 */
@Component
@RequiredArgsConstructor
@RepositoryEventHandler(Transaction.class)
public class TransacationEventHandler {

    private final CurrencyUnitService currencyUnitService;

    @HandleBeforeSave
    @HandleBeforeCreate
    public void handleEvent(Transaction transaction) {
        CurrencyUnit userCurrency = currencyUnitService.getUserCurrencyUnit();

        if (transaction.getFinalAmount() == null) {
            adjustFinalAmount(transaction, userCurrency);
        }
    }

    private void adjustFinalAmount(Transaction transaction, CurrencyUnit userCurrency) {
        if (isTransactionCurrencyEqualsToUser(transaction, userCurrency)) {
            transaction.setFinalAmount(transaction.getAmount());
            transaction.setAmount(null);
        } else {
            transaction.setFinalAmount(
                    MonetaryConversions.getConversion(userCurrency).apply(transaction.getAmount())
            );
        }
    }

    private boolean isTransactionCurrencyEqualsToUser(Transaction transaction, CurrencyUnit userCurrency) {
        return transaction.getAmount().getCurrency().equals(userCurrency);
    }

}
