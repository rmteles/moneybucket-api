package com.teless.moneybucket.model.repository.handler;

import com.teless.moneybucket.model.domain.Account;
import com.teless.moneybucket.model.service.CurrencyUnitService;
import lombok.RequiredArgsConstructor;
import org.javamoney.moneta.FastMoney;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeLinkSave;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

/**
 * Created by Rafael <rmteles@gmail.com> on 20-Jan-17.
 */
@Component
@RequiredArgsConstructor
@RepositoryEventHandler(Account.class)
public class AccountEventHandler {

    private final CurrencyUnitService currencyUnitService;

    @HandleBeforeSave
    @HandleBeforeCreate
    public void handleEvent(Account account) {
        if (account.getBalance() == null) {
            account.setBalance(FastMoney.of(0, currencyUnitService.getUserCurrencyUnit()));
        }
    }

    @HandleBeforeLinkSave
    public void handleLink(Account account, Object linkedObject) {
        if (account.getTransactions() != null) {
            account.getTransactions().forEach(transaction -> transaction.setAccount(account));
        }
    }

}
