package com.teless.moneybucket.model.repository.converter;

import org.javamoney.moneta.FastMoney;

import javax.money.MonetaryAmount;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by Rafael <rmteles@gmail.com> on 16-Jan-17.
 */
@Converter
public class MonetaryAmountConverter implements AttributeConverter<MonetaryAmount, String> {

    @Override
    public String convertToDatabaseColumn(MonetaryAmount amount) {
        if (amount == null) {
            return null;
        }

        return FastMoney.from(amount).toString();
    }

    @Override
    public MonetaryAmount convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }

        return FastMoney.parse(dbData);
    }

}
