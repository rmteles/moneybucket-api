package com.teless.moneybucket.model.domain;

/**
 * Created by rafael on 11/12/16.
 */
public enum TransactionType {

    DEBIT, CREDIT

}
