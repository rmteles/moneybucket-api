package com.teless.moneybucket.model.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.teless.moneybucket.model.repository.converter.MonetaryAmountConverter;
import com.teless.moneybucket.web.converter.MonetaryAmountDeserializer;
import com.teless.moneybucket.web.converter.MonetaryAmountSerializer;
import com.teless.springarq.model.domain.BaseEntity;
import com.teless.springarq.web.converter.LocalDateTimeDeserializer;
import com.teless.springarq.web.converter.LocalDateTimeSerializer;
import lombok.*;

import javax.money.MonetaryAmount;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Created by rafael on 11/12/16.
 */
@Entity
@Data
@Builder
@ToString(exclude = "account")
@EqualsAndHashCode(of = "id", callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "transaction_sq")
public final class Transaction extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "transaction_sq")
    private Long id;

    private String description;

    @NotNull
    private TransactionType type;

    @NotNull
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime date;

    @Convert(converter = MonetaryAmountConverter.class)
    @JsonSerialize(using = MonetaryAmountSerializer.class)
    @JsonDeserialize(using = MonetaryAmountDeserializer.class)
    private MonetaryAmount amount;

    @NotNull
    @Convert(converter = MonetaryAmountConverter.class)
    @JsonSerialize(using = MonetaryAmountSerializer.class)
    @JsonDeserialize(using = MonetaryAmountDeserializer.class)
    private MonetaryAmount finalAmount;

    @ManyToOne
    private Account account;

}
