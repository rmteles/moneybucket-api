package com.teless.moneybucket.model.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.teless.springarq.model.domain.BaseEntity;
import lombok.*;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.List;

/**
 * Created by rafael on 11/12/16.
 */
@Data
@Entity
@Builder
@ToString(exclude = "accounts")
@EqualsAndHashCode(of = "id", callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "user_sq", sequenceName = "user_sq")
public final class User extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "user_sq")
    private Long id;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @Email
    @NotBlank
    @Column(name = "email", unique = true)
    private String email;

    @NotEmpty
    private char[] password;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Account> accounts;

    @JsonIgnore
    public char[] getPassword() {
        return password;
    }

}
