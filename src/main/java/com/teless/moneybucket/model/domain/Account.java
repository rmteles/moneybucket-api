package com.teless.moneybucket.model.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.teless.moneybucket.model.repository.converter.MonetaryAmountConverter;
import com.teless.moneybucket.web.converter.MonetaryAmountDeserializer;
import com.teless.moneybucket.web.converter.MonetaryAmountSerializer;
import com.teless.springarq.model.domain.BaseEntity;
import lombok.*;
import org.hibernate.validator.constraints.NotBlank;

import javax.money.MonetaryAmount;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by rafael on 11/12/16.
 */
@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id", callSuper = false)
@SequenceGenerator(name = "account_sq", sequenceName = "account_sq")
public final class Account extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "account_sq")
    private Long id;

    @NotBlank
    private String name;
    private String description;

    @Convert(converter = MonetaryAmountConverter.class)
    @JsonSerialize(using = MonetaryAmountSerializer.class)
    @JsonDeserialize(using = MonetaryAmountDeserializer.class)
    private MonetaryAmount balance;

    @ManyToOne
    private User owner;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "account", orphanRemoval = true)
    private List<Transaction> transactions;

}
