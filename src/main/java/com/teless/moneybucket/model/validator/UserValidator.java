package com.teless.moneybucket.model.validator;

import com.teless.moneybucket.model.domain.User;
import com.teless.springarq.model.validator.SpringValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

/**
 * Created by rafael on 04/12/16.
 */
@Component
public class UserValidator extends SpringValidator<User> {

    private static final int PASSWORD_MIN_LENGTH = 8;
    private static final int PASSWORD_MAX_LENGTH = 20;
    private static final Integer[] PASSWORD_VALID_RANGE = {PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH};

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validateObject(User user, Errors errors) {
        String passwordStr = String.valueOf(user.getPassword());
        if (user.getPassword().length < PASSWORD_MIN_LENGTH
                || user.getPassword().length > PASSWORD_MAX_LENGTH
                || !passwordStr.matches(".*[a-zA-Z]+.*")
                || !passwordStr.matches(".*[0-9]+.*")) {

            errors.rejectValue("password", "user.error.password.invalid", PASSWORD_VALID_RANGE, "error.invalid");
        }
    }
}
