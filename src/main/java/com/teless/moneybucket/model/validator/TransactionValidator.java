package com.teless.moneybucket.model.validator;

import com.teless.moneybucket.model.domain.Transaction;
import com.teless.springarq.model.validator.SpringValidator;
import org.springframework.validation.Errors;

/**
 * Created by Rafael <rmteles@gmail.com> on 16-Jan-17.
 */
public class TransactionValidator extends SpringValidator<Transaction> {

    @Override
    public boolean supports(Class<?> clazz) {
        return Transaction.class.equals(clazz) ;
    }

    @Override
    public void validateObject(Transaction transaction, Errors errors) {
        if (transaction.getAmount() != null && transaction.getAmount().isNegative()) {
            errors.rejectValue("amount", "transaction.error.invalid.amount", "error.invalid");
        }
    }

}
