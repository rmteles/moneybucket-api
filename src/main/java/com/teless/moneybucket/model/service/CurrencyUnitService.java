package com.teless.moneybucket.model.service;

import org.springframework.stereotype.Service;

import javax.money.CurrencyUnit;
import javax.money.Monetary;

/**
 * Created by Rafael <rmteles@gmail.com> on 16-Jan-17.
 */
@Service
public class CurrencyUnitService {

    private static final CurrencyUnit CURRENCY_UNIT = Monetary.getCurrency("BRL");

    public CurrencyUnit getUserCurrencyUnit() {
        return CURRENCY_UNIT;
    }

}
