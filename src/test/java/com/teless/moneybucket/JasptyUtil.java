package com.teless.moneybucket;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Rafael <rmteles@gmail.com> on 13-Feb-17.
 */
public class JasptyUtil {

    private StandardPBEStringEncryptor encryptor;

    @Before
    public void setUp() throws Exception {
        encryptor = new StandardPBEStringEncryptor();
        encryptor.setAlgorithm("PBEWithMD5AndDES");
        encryptor.setPassword(System.getProperty("encryptPassword"));
    }

    @Test
    public void encrypt() {
        String result = encryptor.encrypt(System.getProperty("valueToEncrypt"));
        System.out.println(result);
    }

    @Test
    public void decrypt() {
        String result = encryptor.decrypt(System.getProperty("valueToDecrypt"));
        System.out.println(result);
    }

}
